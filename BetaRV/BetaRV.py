
import math

#--------------------- Define Beta Random Variable Class ---------------------
class BetaRandomVariable:
  def __init__(self):
    self.alpha = []
    self.beta = []
    self.representative = []
    self.num_obs = []
    self.validity = []

  def fit(self,vector): # How to fit a vector with a beta random variable
    totalRatings = len(vector)
    self.alpha = 1
    for n in range( totalRatings) :
      self.alpha = self.alpha - ( vector[n] * math.log(1.0/(2*totalRatings) + (n + 0.0)/totalRatings));

    self.beta = 1
    for n in range( totalRatings ) :
      self.beta = self.beta - ( vector[n] * math.log( (2.0 * totalRatings - 1 ) / (2 * totalRatings) - ( n + 0.0)/totalRatings))
    
    self.calc_beta_rep()

  def calc_beta_rep(self, rep = 'Mean'):
    if rep == 'Mean':         #------------------------ Mean
      self.representative = (self.alpha / (self.alpha + self.beta ) )
    elif rep == 'Median':     #------------------------ Median
      self.representative = ( ( self.alpha - (1/3) ) / (self.alpha + self.beta - (2/3) ) )
    elif rep == 'Mode':       #------------------------ Mode
      self.representative = ( (self.alpha - 1) / (self.alpha + self.beta - 2 ) )
    
    #print('Mean: ' + str(self.representative) )