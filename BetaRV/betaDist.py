import numpy as np
from sklearn.model_selection import KFold
from scipy.stats import beta
import matplotlib.pylab as plt

from math import *


def betaDist(vec):
    alpha = 1
    size = len(vec)
    for n in range (1,size):
        alpha = alpha - ( vec[n]*log( 1.0/(2*size) + (n-1)/size ) ) 
    
    beta = 1
    for n in range (1,size):
        beta = beta - ( vec[n]*log( (2*size -1)/(2*size) - (n-1)/size ) ) 
    return (alpha , beta)


a, b = 1., 2.

x = beta.rvs(a, b, size=100)
a1, b1, loc1, scale1 = beta.fit(x)

a2 , b2 = betaDist(x)

print(a)
print(b)
print(a1)
print(b1)

print(a2)
print(b2)
