import numpy as np
import pandas as pd 


dic = dict()
genre = pd.read_csv('dataset/ML-100K/genre.csv').values.tolist()

for item in genre:
    dic[ item[0] ] = item[1]
print(dic)

#output_file = open('dataset/ML-100K/movies')

movies_list = list()
movies_list.append(['MovieID','MovieName','Genre'])

with open('dataset/ML-100K/u.item') as fp:
    line = fp.readline()
    while line:
        #print(line)
        line = line.split("|")
        if line[0] == '':
            break 
        try:       
            movieID = line[0]
            movieName = line[1]
            g = list()
            for i in range(5,24):
                if int(line[i]):
                    g.append( dic[i-5] )
            s = "|"
            g = s.join(g)
            movies_list.append([movieID,movieName,g] )
        except Exception as ex:
            print(ex)
        line = fp.readline()
        
    
    pd.DataFrame(movies_list).to_csv('dataset/ML-100K/movies.csv' , index=False)
    




