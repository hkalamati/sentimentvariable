

"""
from openpyxl.workbook import Workbook

wb = Workbook()

ws1 = wb.create_sheet("Sheet_A")
ws1.title = "Title_A"

ws2 = wb.create_sheet("Sheet_B", 0)
ws2.title = "Title_B"

wb.save(filename = 'sample_book.xlsx')

a = wb.sheetnames

print(a)
"""

"""import pandas as pd
import numpy as np

path = r"PhD_data.xlsx"

x1 = np.random.randn(100, 2)
df1 = pd.DataFrame(x1)

x2 = np.random.randn(100, 2)
df2 = pd.DataFrame(x2)

writer = pd.ExcelWriter(path, engine = 'xlsxwriter')
df1.to_excel(writer, sheet_name = 'x1')
df2.to_excel(writer, sheet_name = 'x2')
writer.save()
writer.close()
"""

# import xlsxwriter module 
import xlsxwriter 
from openpyxl import load_workbook

# Workbook() takes one, non-optional, argument  
# which is the filename that we want to create. 
#workbook = xlsxwriter.Workbook('hello.xlsx') 
workbook = load_workbook('hello.xlsx')  
# The workbook object is then used to add new  
# worksheet via the add_worksheet() method. 
worksheet = workbook.create_sheet("f3") 
worksheet.title = "f3"

worksheet.cell(row = 1 , column = 1).value = 1
workbook.save('hello.xlsx')
# Use the worksheet object to write 
# data via the write() method. 
#worksheet.write('A1', 'Hello..') 
#worksheet.write('B1', 'Geeks') 
#worksheet.write('C1', 'For') 
#worksheet.write('D1', 'Geeks') 

# Finally, close the Excel file 
# via the close() method. 
#workbook.close() 
