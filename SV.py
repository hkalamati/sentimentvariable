# -*- coding: utf-8 -*-

import os 
import csv
import pandas as pd
import numpy as np
from sklearn.model_selection import KFold
from sklearn.utils import shuffle
from scipy.stats import beta
from scipy.stats.stats import pearsonr
from collections import Counter
import math
#import matplotlib.pyplot as plt
import random
from copy import deepcopy
import time
from openpyxl import load_workbook
from openpyxl.workbook import Workbook


random.seed(1)

class Config:
  def __init__(self):
    self.max_rate = []
    self.min_rate = []
    self.num_cat = []
    self.num_knn = []
    self.num_movies = []
    self.num_users = []
    self.similarity_measure = ''
    self.weight_func = ''
    self.num_valid_rated_item_each_cat = []
    self.debug = []
    self.dataset_name = ''
    self.ratings_path = ''
    self.movies_path = ''
    self.genres_path = ''
    self.time_path = ''
    self.plot = ''
    
    self.initilization()

  def initilization(self):
    self.plot = True
    self.max_rate = 5
    self.min_rate = 1
    self.num_knn  = 10
    self.similarity_measure = 'Pearson'
    #self.weight_func = 'gHerlocker'
    self.weight_func = 'ArcTan'
    #self.weight_func = 'gHerlocker'
    #self.weight_func = 'gArcTan'
    self.num_valid_rated_item_each_cat = 1
    self.debug = False
    
    self.dataset_name = 'ML-100K'
    #self.dataset_name = 'ML-1M'


#--------------------- Define Beta Random Variable Class ---------------------
class BetaRandomVariable:
  def __init__(self):
    self.alpha = []
    self.beta = []
    self.representative = []
    self.num_obs = []
    self.validity = []

  def fit(self,vector): # How to fit a vector with a beta random variable
    totalRatings = len(vector)
    self.alpha = 1
    for n in range( totalRatings) :
      self.alpha = self.alpha - ( vector[n] * math.log(1.0/(2*totalRatings) + (n + 0.0)/totalRatings));

    self.beta = 1
    for n in range( totalRatings ) :
      self.beta = self.beta - ( vector[n] * math.log( (2.0 * totalRatings - 1 ) / (2 * totalRatings) - ( n + 0.0)/totalRatings))
    
    self.calc_beta_rep()

  def calc_beta_rep(self, rep = 'Median'):
    if rep == 'Mean':         #------------------------ Mean
      self.representative = (self.alpha / (self.alpha + self.beta ) )
    elif rep == 'Median':     #------------------------ Median
      self.representative = ( ( self.alpha - (1/3) ) / (self.alpha + self.beta - (2/3) ) )
    elif rep == 'Mode':       #------------------------ Mode
      self.representative = ( (self.alpha - 1) / (self.alpha + self.beta - 2 ) )
    
    #print('Mean: ' + str(self.representative) )

#--------------------- Define Similarity Class ---------------------

class Similarity:
  def __init__(self):
    self.user = []
    self.value = []
    self.num_coRated_items = []

#--------------------- Define Data Model Class ---------------------

class DataModel:
  def __init__(self):
    self.rating_matrix = []
    self.similarity_matrix = []
    self.users_avg_matrix = []
    self.black_movies = []
    self.black_users = []
    self.num_rates = []

#--------------------- Define Error Class ---------------------

class Error:
  def __init__(self):
    self.MAE = []
    self.MSE = []
    self.num_obs = []
    self.total_error = []

  def calculate_total_error(self):
    self.total_error = np.sqrt( np.power(self.MAE , 2 ) + self.MSE  )

data_model = DataModel()
similarity = Similarity()
config = Config()

###############################################################################################################

#--------------------- Create Directory ---------------------
def create_dir(dir_addr):
    dir_list = dir_addr.split('/')
    print(dir_list)

    var = ''
    for item in dir_list:
        var = var + item
        if not os.path.isdir(var) and '.' not in var :
            os.mkdir(var)
        var += '/'

#--------------------- Split Dataset ---------------------
def train_test(train_indices ,test_indices ):
  return (ratings[train_indices] , ratings[test_indices] )

#--------------------- Initialize Rating Matrix ---------------------
def initial_rating_matrix(train_data):
  print('Initial Rating Matrix...')
  rating_matrix = np.empty(( config.num_users , config.num_movies ) )
  rating_matrix[:] = np.nan

  rated_count = np.zeros(config.num_movies)
  data_model.num_rates = np.zeros(config.num_users)
  
  for i in range(len(train_data)):
    user  = int( train_data[i , 0 ] )
    movie = int( train_data[i , 1 ] )
    rate  = int( train_data[i , 2 ] )
    rating_matrix[ user  ][ movie  ] = rate

    rated_count[movie] = rated_count[movie] + 1
    data_model.num_rates[user] += 1

  data_model.black_movies = np.empty(config.num_movies)

  for i in range(config.num_movies):
    if rated_count[i] < config.num_knn :
      data_model.black_movies[i] = True
    else:
      data_model.black_movies[i] = False

  return rating_matrix

#--------------------- Initialize User Category Matrix ---------------------
def initial_user_category_matrix():
  print("initial_user_category_matrix Function")
  betaRV = BetaRandomVariable()

  user_category_matrix = [ [ BetaRandomVariable() for j in range( config.num_cat ) ] for i in range( config.num_users )]

  config.num_rates = config.max_rate - config.min_rate + 1

  rating_matrix_df = pd.DataFrame(data_model.rating_matrix)
  notnull_ratings_list = list( rating_matrix_df[rating_matrix_df.notnull()].stack().index )
  
  for i in range(config.num_users):
    if config.plot : 
      print(f"User Category-User {i}")
    if config.debug == True:
      if i == 20: 
        break
    
    rated_index = [ item for item in notnull_ratings_list  if item[0] == i ]
    for genre in data_model.genres_list:
      ratings = [ data_model.rating_matrix[i][ item[1] ] for item in rated_index if genre in movies_genre_dic[ item[1] ] ] 
      value_counts= Counter(ratings)
      x = [value_counts[1] , value_counts[2] , value_counts[3] , value_counts[4] , value_counts[5]]  
      try:
        y = x / np.max(x)
      except Exception as ex:
        print(ex)
      #print(x)
      #print(y)
      try:
        betaRV = BetaRandomVariable()
        betaRV.fit(y)
        #print(betaRV.alpha , betaRV.beta)
        betaRV.num_obs = np.sum(x)
        betaRV.validity = True
        if betaRV.num_obs < config.num_valid_rated_item_each_cat :
          betaRV.validity = False
        
      except Exception as ex:
        print(ex)
        betaRV.validity = False 

      #print(betaRV.validity)
      #if betaRV.validity:
      user_category_matrix[i][ data_model.genre_dict[genre] ] = betaRV
      #user_category_matrix[i][ data_model.genre_dict[genre] ] = betaRV.get_beta_rep()
        #print('OKKKK')
      #else:
      #  user_category_matrix[i][ data_model.genre_dict[genre] ] = np.nan
        #print('No!!!')

  return user_category_matrix

#--------------------- Calculate Average rates of users ---------------------
def calculate_avg_matrix():
  print('Calculate average rates of allusers...')
  avg_matrix = np.empty( config.num_users)
  avg_matrix[:] = np.nan

  for i in range(config.num_users):
    if config.plot :
      print(f"Average-User {i}")
    if config.debug == True:
      if i == 20 : 
        break
    data = list()
    for j in range(config.num_cat):
      #print(data_model.user_category_matrix[i][j].validity) 
      if data_model.user_category_matrix[i][j].validity :
        data.append( data_model.user_category_matrix[i][j].representative )
    try:
      avg_matrix[i] = np.average(data)
    except:
      avg_matrix[i] = np.nan
    #print(avg_matrix[i])
  
  return (avg_matrix)  

#--------------------- Prepare vectors ---------------------
def prepare_vectors(vec_1 , vec_2):
  x = list()
  y = list()

  for i in range(len(vec_1)):
    if vec_1[i].validity and vec_2[i].validity:
      x.append(vec_1[i].representative)
      y.append(vec_2[i].representative)
  return (x,y)

#--------------------- Weight ---------------------

"""def co_rated_weight(x):
  return 1"""

def weight(x):
  
  if config.weight_func == 'NoSW':
    W = 1
  elif config.weight_func == 'ArcTan':
    W = ( (2 / np.pi)  * np.arctan(x))
  elif config.weight_func == 'gArcTan':
    if config.dataset_name == 'ML-100K':
      z = 0.01
      y = 0.05
    elif config.dataset_name == 'ML-1M':
      z = 0.01
      y = 0.05
    W = np.max( [ 0 , z + (1-z) * ( (2 / np.pi)  * np.arctan(y * x)) ] )
  
  elif config.weight_func == 'Sig':
    W = 1.0 / ( 1 + np.exp(-x/2) )
  elif config.weight_func == 'gSig':
    if config.dataset_name == 'ML-100K':
      z = 0.001
      y = 0.04
    elif config.dataset_name == 'ML-1M':
      z = 0.001
      y = 0.04
    W = np.max( [ 0 , ( 1 / (z + 0.5) ) * ( ( z-0.5 ) + ( 1.0 / ( 1 + np.exp(-y * x) ) ) ) ] )
  
  elif config.weight_func == 'Koren':
    if config.dataset_name == 'ML-100K':
      Thr = 30
    elif config.dataset_name == 'ML-1M':
      Thr = 30
    W = ((x) / (x + Thr))
  elif config.weight_func == 'gKoren':
    if config.dataset_name == 'ML-100K':
      z = 0.015
      y = 15
    elif config.dataset_name == 'ML-1M':
      z = 0.015
      y = 15
    W = np.max( [ 0 , z + (1-z) * ( (x) / (x + y) ) ] )
  return W

#--------------------- Calculate Similarity of Users ---------------------
def calculate_similarity_matrix():
  print("Calculate Similarity Matrix...")
  similarity_matrix = [ [ Similarity() for j in range( i, config.num_users ) ] for i in range( config.num_users )]
  print('Start...')
  for i in range(config.num_users):
    if config.plot : 
      print(f"Similarity-User {i}")
    if config.debug == True:
      if i == 20:
        break
    for j in range( config.num_users - i ):
      if config.debug == True:
        if j == 20:
          break
      x , y = prepare_vectors( data_model.user_category_matrix[i] , data_model.user_category_matrix[ i + j])
      #bad = ~np.logical_or(np.isnan(x), np.isnan(y))
      #x = np.compress(bad, x)  
      #y = np.compress(bad, y)
      try:
        similarity_matrix[i][j].value = weight( data_model.num_rates[i] ) * weight(data_model.num_rates[j]) * pearsonr(x,y)[0]
        similarity_matrix[i][j].user  = i + j 
        similarity_matrix[i][j].num_coRated_items = len(x)
      except:
        similarity_matrix[i][j].value = np.nan
        similarity_matrix[i][j].user  = i + j

  return (similarity_matrix)

#--------------------- Validation of Test Cases ---------------------
def validation_of_test_cases(test_data , iteration):
  print('Validation of Test Set...')

  file_name = 'Valid_Data/' + config.dataset_name + '/valid_data_iteration_' + str(iteration) + '_' + str( config.num_knn) + '.csv'
  create_dir(file_name)

  if os.path.isfile(file_name):
    with open(file_name, newline='') as f:
      reader = csv.reader(f)
      valid_data = list(reader)
    
    valid_data = [list( map(int,item) ) for item in valid_data] 
    return valid_data
  try:
    test_data = test_data[np.argsort(test_data[:,0])]
  except Exception as ex:
    print(ex)

  valid_data = list()
  
  last_user = -1
  debug_counter = 0
  
  if len(data_model.similarity_matrix) == 943 and len(data_model.similarity_matrix[5]) == 943:
    stop = 0 
  for i in range( len(test_data) ):
    user = test_data[i][0]
    item = test_data[i][1]
    #rate = test_data[i][2]
    
    if data_model.black_movies[ item ]:
      continue

    if last_user != user:
      if config.plot : 
        print(f"Validation-User {user}")
      last_user = user
      similar_users_vector = []
      similar_users_vector = deepcopy( data_model.similarity_matrix[user][:] )
      x = np.arange(0, user , 1)
      y = np.arange(user , 0 , -1)
      try:
        for i , j in zip(x,y):
          similar_user = []
          similar_user = deepcopy( data_model.similarity_matrix[i][j] )
          if similar_user.user != user:
            print(f"Wrong Similar User - i={i} , j={j} and similar.user = {similar_user.user}:")
          similar_user.user = i
          similar_users_vector.append( similar_user )
      except Exception as ex:
        print(ex)

      similar_users_vector = [ inst for inst in  similar_users_vector if ~np.isnan( inst.value )  ]
      similar_users_vector.sort(key = lambda x: x.value , reverse = True)
    
    summ = 0
    sumSimilarity = 0
    average = data_model.users_avg_matrix[ user ]

    if np.isnan(average) or average == 0:
      continue
    
    num_valid_neighbors = 0
    validation = True
    counter = 0 
    
    while ( counter < len(similar_users_vector) and validation and num_valid_neighbors < config.num_knn):
      
      similar_user = similar_users_vector[counter]
      counter += 1


      if (similar_user.user == user or data_model.rating_matrix[ similar_user.user ][ item ] == 0 or np.isnan(data_model.rating_matrix[ similar_user.user ][ item ] ) ):
        continue
      
      avg_neighbor = data_model.users_avg_matrix[ similar_user.user ]

      summ += ( similar_user.value * (data_model.rating_matrix[ similar_user.user ][item] - avg_neighbor ) )
      sumSimilarity += np.abs(similar_user.value)

      if (sumSimilarity == 0):
        validation = False
        continue
      
      num_valid_neighbors += 1

    if (np.isnan(sumSimilarity) or validation == False or num_valid_neighbors < config.num_knn ):
      continue
    
    valid_data.append( [ int(test_data[i][0]) , int(test_data[i][1]) , int(test_data[i][2]) ] )
    
    debug_counter += 1
    if config.debug and debug_counter == 10:
      break
  
  with open(file_name , 'w' , newline='') as file:
    write = csv.writer(file)
    write.writerows(valid_data)
  
  return valid_data
  
#--------------------- Prediction ---------------------

def prediction(valid_test_data , iteration):
  print("Execute Prediction phase...")

  file_name = 'result/' + config.dataset_name  + '/' + config.weight_func + '/' + str( config.similarity_measure ) \
   + '_' + str( config.weight_func ) + '_' + str( config.num_knn ) + '.xlsx'

  create_dir(file_name)

  valid_test_data = np.array(valid_test_data)
  valid_test_data = valid_test_data[np.argsort(valid_test_data[:,0])]

  time_vector = np.zeros(config.num_knn)
  error_vector = [ Error() for ii in range( config.num_knn )]
  for ii in range(config.num_knn): 
    error_vector[ii].MSE = 0
    error_vector[ii].MAE = 0
    error_vector[ii].num_obs = 0

  last_user = -1    
  for test_case in valid_test_data:
    t1 = time.time()

    user = test_case[0]
    item = test_case[1]
    rate = test_case[2]

    if data_model.black_movies[ item ]:
      continue

    if last_user != user:
      if config.plot :   
        print(f"Prediction-User {user}")
      last_user = user
      similar_users_vector = []
      similar_users_vector = deepcopy( data_model.similarity_matrix[user][:] )
      x = np.arange(0, user , 1)
      y = np.arange(user , 0 , -1)
      try:
        for i , j in zip(x,y):
          similar_user = []
          similar_user = deepcopy( data_model.similarity_matrix[i][j] )
          if similar_user.user != user:
            print(f"Wrong Similar User - i={i} , j={j} and similar.user = {similar_user.user}:")
          similar_user.user = i
          similar_users_vector.append( similar_user )
      except Exception as ex:
        print(ex)
      
      similar_users_vector = [ inst for inst in  similar_users_vector if ~np.isnan( inst.value ) ]
      similar_users_vector.sort(key = lambda x: x.value , reverse = True)

    summ = 0
    sumSimilarity = 0
    average_target_user = data_model.users_avg_matrix[ user ]

    num_valid_neighbors = 0
    counter = 0

    t2 = time.time() - t1
    
    sum_time = 0 
    while ( num_valid_neighbors < config.num_knn and counter < len(similar_users_vector) ):

      t3 = time.time()  

      similar_user = similar_users_vector[counter]
      counter += 1
    
      if (similar_user.user == user or data_model.rating_matrix[ similar_user.user ][ item ] == 0 or np.isnan(data_model.rating_matrix[ similar_user.user ][ item ] ) ):
        continue
    
      avg_neighbor_user = data_model.users_avg_matrix[ similar_user.user ]

      summ += ( similar_user.value * (data_model.rating_matrix[ similar_user.user ][item] - avg_neighbor_user ) )
      sumSimilarity += np.abs(similar_user.value)

      if (sumSimilarity != 0):
        predicted_rate = average_target_user + ( (1/sumSimilarity) * summ)
      else:
        predicted_rate = average_target_user
      
      if predicted_rate > config.max_rate:
        predicted_rate = config.max_rate
      elif predicted_rate < config.min_rate:
        predicted_rate = config.min_rate
      
      t4 = time.time() - t3
      sum_time += t4
      time_vector[num_valid_neighbors] += sum_time

      err = rate - predicted_rate
      error_vector[num_valid_neighbors].MAE += np.abs( err )
      error_vector[num_valid_neighbors].MSE += np.power(err ,2)
      error_vector[num_valid_neighbors].num_obs += 1
          
      num_valid_neighbors += 1
  
  error_list = list()
  error_list.append(['' , 'MAE' , 'MSE' , 'TotalError' , 'num_obs' , 'time'])
  for i in range(config.num_knn):
    error_vector[i].MAE = (1.0 * error_vector[i].MAE ) / error_vector[i].num_obs
    error_vector[i].MSE = (1.0 * error_vector[i].MSE ) / error_vector[i].num_obs
    error_vector[i].calculate_total_error()
    mean_time = (1.0 * time_vector[i] ) / error_vector[i].num_obs
    error_list.append([ i + 1 , error_vector[i].MAE , error_vector[i].MSE , error_vector[i].total_error , error_vector[i].num_obs , mean_time ])

  if os.path.isfile(file_name):
    workbook = load_workbook(file_name)
  else:
    workbook = Workbook()

  worksheet_name = 'Fold ' + str(iteration)
  worksheet = workbook.create_sheet(index = iteration)
  worksheet.title = worksheet_name

  row = 1
  col = 1
  for item in error_list[0]:
    worksheet.cell(row = row, column = col).value = item
    col += 1
  
  for i in range(2 , config.num_knn + 2 ):
    for j in range(1 , len(error_list[i-1]) + 1 ):
      worksheet.cell( row = i , column = j ).value = error_list[i-1][j-1] 

  workbook.save(file_name)

#--------------------- Write Results ---------------------

def average_of_k_fold():
  file_name = 'result'
  file_name = file_name + '/' + config.dataset_name
  file_name = file_name + '/' + config.weight_func
  file_name = file_name + '/' + str( config.similarity_measure ) + '_' + str( config.weight_func ) + '_' + str( config.num_knn ) + '.xlsx'
  
  workbook = load_workbook(file_name)

  if 'Average' in workbook.sheetnames:
    return

  summ = np.zeros((config.num_knn , 5))

  sw = True
  ii = 1
  while sw:
    worksheet_name = 'Fold ' + str(ii)
    if worksheet_name not in workbook.sheetnames:
      sw = False
      continue
    ws = workbook.get_sheet_by_name(worksheet_name)

    for r in range(2 , config.num_knn + 2):
      for c in range(2,7):
        summ[r-2][c-2] += ws.cell(row = r , column = c).value
    
    ii += 1
  
  avg_sheet = workbook['Sheet']
  avg_sheet.title = 'Average'
  
  title = ['' , 'MAE' , 'MSE' , 'TotalError' , 'num_obs' , 'time']
  row = 1
  col = 1
  for item in title:
    avg_sheet.cell(row = row, column = col).value = item
    col += 1
  row = 2
  col = 1
  for i in range( config.num_knn ):
    avg_sheet.cell(row = row, column = col).value = row - 1
    row += 1
  
  for r in range( config.num_knn ):
    for c in range( len(summ[r] ) ):
      avg_sheet.cell( row = r + 2 , column = c + 2 ).value = ( 1.0 * summ[ r ][ c ] ) /  ( len( workbook.sheetnames) - 1 )

  workbook.save(file_name)

  #with open(file_name , 'w' , newline='') as file:
  #  write = csv.writer(file)
  #  write.writerows(error_list)

#weights = ['NoSW' , 'ArcTan', 'Sig', 'gSig', 'Koren', 'gKoren']
#data_sets = [ 'ML-100K' ,  'ML-1M']

weights = ['NoSW' , 'ArcTan' , 'gArcTan' , 'Sig', 'gSig', 'Koren', 'gKoren']
#data_sets = [ 'ML-1M' , 'ML-100K' ]
data_sets = [ 'ML-100K' , 'ML-1M' ]

for dataset in data_sets:
    config.dataset_name = dataset
    config.ratings_path = 'dataset/' + dataset +'/ratings.csv'
    config.movies_path  = 'dataset/' + dataset +'/movies.csv'
    config.genres_path  = 'dataset/' + dataset +'/genre.csv'
    config.time_path    = 'result/'  + dataset +'/time.csv'
    
    for item in weights:
        config.weight_func = item


        #####################################
        ratings_df = pd.read_csv(config.ratings_path )
        #ratings_df = shuffle(ratings_df)
        #ratings_df
        #-----------------------------------------------
        movies_df = pd.read_csv(config.movies_path)
        #movies_df

        #####################################
        user_list =  list( ratings_df['UserID']  ) 
        movie_list = list( movies_df['MovieID'] )

        config.num_users  = len(np.unique(user_list))
        config.num_movies = len(np.unique(movie_list)) 

        #-----------------------------------------------
        user_dic  = dict()
        movie_dic = dict()

        i = 0
        for item in user_list:
          if item not in user_dic:
            user_dic[item] = i 
            i = i + 1

        i = 0
        for item in movie_list:
          if item not in movie_dic:
            movie_dic[item] = i 
            i = i + 1

        def transform(x , dict_var):
            return dict_var[x]

        movies_df["MovieID"]  = movies_df["MovieID"].apply(transform , args = (movie_dic,))
        ratings_df["MovieID"] = ratings_df["MovieID"].apply(transform , args = (movie_dic,) )
        ratings_df["UserID"]  = ratings_df["UserID"].apply(transform , args = (user_dic,) )


        MovieID = list( movies_df['MovieID'] )
        genres  = list( movies_df['Genre'] )

        movies_genre_dic = dict()

        for i in range( config.num_movies ):
          movies_genre_dic[  MovieID[i]  ] = genres[i].split('|')

        genres = pd.read_csv(config.genres_path)
        config.num_cat = len(genres)

        data_model.genres_list = genres['genre_name'].values.tolist()

        temp = genres.values.tolist()
        data_model.genre_dict = dict()

        if temp[0][0] == 0:
          for item in temp:
            data_model.genre_dict[ item[1] ] = item[0]
        else:     
          for item in temp:
            data_model.genre_dict[ item[1] ] = item[0] - 1

        #data_model.genre_dict

        #####################################
        ratings = np.array( ratings_df.values.tolist() )
        indices = np.arange( len(ratings) )
        random.shuffle(indices)
        ratings = ratings[indices]

        #Random(4).shuffle(ratings)

        file_time = 'result'
        if not os.path.isdir(file_time):
          os.mkdir(file_time)
        file_time = file_time + '/' + config.dataset_name
        if not os.path.isdir(file_time):
          os.mkdir(file_time)
        file_time = file_time + '/' + config.weight_func
        if not os.path.isdir(file_time):
          os.mkdir(file_time)

        kf = KFold(n_splits = 5 )

        iteration = 1

        time_df = [ [ 'User_Category_time' , 'Similarity_time' ] ]

        for train_indices, test_indices in kf.split(ratings):
          print('---------------------------------------------------------------------------')
          print(f"DataSet {config.dataset_name} - Weight Function {config.weight_func} - Fold {iteration}")

          file_name = 'result/' + config.dataset_name + '/' + config.weight_func
          file_name = file_name + '/' + str( config.similarity_measure ) + '_' + str( config.weight_func ) + '_' + str( config.num_knn ) + '.xlsx'

          if os.path.isfile(file_name):
            workbook = load_workbook(file_name)
            worksheet_name = 'Fold ' + str(iteration)
            if worksheet_name in workbook.sheetnames:
              iteration += 1
              continue

          #----------------------- Create Train and Test Sets
          train_data , test_data = train_test(train_indices , test_indices)
          #print("train: " + str(len(train_data)))
          #print("test: " + str(len(test_data)))

          #----------------------- Create User_Item Matrix Using Training Set
          data_model.rating_matrix = initial_rating_matrix(train_data)

          #----------------------- Create User_Category Matrix Using Training Set
          user_category_start_time = time.time()
          data_model.user_category_matrix = initial_user_category_matrix()
          user_category_duration_time = time.time() - user_category_start_time 

          #----------------------- Create Ratings Average Matrix for all Users
          data_model.users_avg_matrix = calculate_avg_matrix()

          #----------------------- Create Similarity Matrix
          similarity_start_time = time.time()
          data_model.similarity_matrix = calculate_similarity_matrix()
          similarity_duration_time = time.time() - similarity_start_time

          #----------------------- Validation of test cases
          valid_test_data = validation_of_test_cases(test_data, iteration)

          #----------------------- Prediction Phase
          prediction(valid_test_data , iteration)

          time_df.append([ user_category_duration_time , similarity_duration_time])

          iteration += 1

          #break

        if iteration <= 5:
          time_path = file_time + '/time_userCat_SimMat_' + str( config.similarity_measure ) + '_' \
          + str( config.weight_func ) + '_' + str( config.num_knn ) + '_iteration_' + str(iteration) + '.csv'
          time_df = pd.DataFrame(time_df)
          header = time_df.iloc[0]
          time_df = time_df[1:]
          time_df.columns = header
          time_df.to_csv(time_path , index=False)
          
        average_of_k_fold()

print('Your Runnig finished')

