
import os
from openpyxl import load_workbook
from openpyxl.workbook import Workbook

#core_path = "C:/Users/Hadi/OneDrive/Thesis Paper/SVpaper/Results/ML-100K"
core_path = "C:/Users/Hadi/OneDrive/Thesis Paper/SVpaper/Results/ML-1M"
sim_measure = 'Pearson'
knn = 10

final_result_file = core_path + '/' + 'Agregated_results.xlsx'
result_workbook = Workbook()
res_sheet = result_workbook['Sheet']
res_sheet.title = 'results'

row = 2
col = 1
for i in range( knn ):
    res_sheet.cell(row = row, column = col).value = row - 1
    row += 1

print(os.listdir(core_path))

col = 2
for item in os.listdir(core_path):
    file_name = core_path + '/' + item + '/' + sim_measure + '_' + item + '_'  + str(knn) + '.xlsx'
    
    temp = core_path + '/' + item
    
    if not os.path.isdir(temp):
        continue

    workbook = load_workbook(file_name)

    if 'Average' not in workbook.sheetnames:
        print('Error in this file: ' + file_name)
    
    ws = workbook.get_sheet_by_name('Average')
    
    res_sheet.cell(row = 1 , column = col ).value = item
    
    for i in range( knn ):
        res_sheet.cell(row = i + 2 , column = col ).value = ws.cell(row = i+2 , column = 4).value
    
    col += 1
   

result_workbook.save(final_result_file)








